const mongoose = require('mongoose');

const CartSchema = new mongoose.Schema({

user: { 
    type : String,

},

orders : {
    type : Array,
    default : []
},

})
module.exports = mongoose.model("Cart", CartSchema);