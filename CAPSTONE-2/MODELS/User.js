const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName : {
		type : String,
		required : [true, "First name is required"]
	},
	lastName : {
		type : String,
		required : [true, "Last name is required"]
	},
	email : {
		type : String,
		required : [true, "Email is required"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	mobileNo : {
		type : String, 
		required : [true, "Mobile No is required"]
    },
orderedProduct: [{
	products: [{
		productId: {
			type: String,
			required: true,
		},
		productName: {
			type: String,
			required: true,
		},
		quantity: {
			type: Number,
			required: true,
		}
	}],
	totalAmount: {
		type: Number,
		required: true,
	},
	PurchasedOn: {
		type: Date,
		default: new Date()
	}
}]
})


module.exports = mongoose.model('User', userSchema)