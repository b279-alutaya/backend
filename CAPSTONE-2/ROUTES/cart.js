const express = require("express");
const router = express.Router();
const auth = require("../auth.js")
const cartController = require("../CONTROLLERS/cartController");

router.post('/', auth.verify, cartController.createCart);

module.exports = router;