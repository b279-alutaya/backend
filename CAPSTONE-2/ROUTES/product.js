const express = require("express");
const router = express.Router();
const auth = require("../auth.js")
const productController = require("../CONTROLLERS/productController");

// CREATE PRODUCT
router.post("/Create", (req, res) => {
    let data = {
        userId : auth.decode(req.headers.authorization).id,
        isAdmin : auth.decode(req.headers.authorization).isAdmin,
        product : req.body
    }
	productController.createProduct(data).then(resultFromController => res.send(resultFromController));
});

// GET ALL PRODUCTS
router.get("/all", (req, res) => {
	productController.getAllProduct().then(resultFromController => res.send(resultFromController));
})
// GET ALL ACTIVE PRODUCTS
router.get("/allactive", (req, res) => {
	productController.getAllActiveProduct().then(resultFromController => res.send(resultFromController));
})
// GET SPECIFIC PRODUCT
router.get("/:productId", (req, res) => {
	console.log(req.params.productId);
	productController.getSpecificProduct(req.params).then(resultFromController => res.send(resultFromController));

})

// Edit specific product
router.put("/:productId", auth.verify, (req, res) => {
    let isAdmin = auth.decode(req.headers.authorization).isAdmin;
    productController.updateProduct(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));

})
// UPDATE PRODUCT INFO
router.put("/:productId", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.archiveCourse(req.params, isAdmin).then(resultFromController => res.send(resultFromController));
	
});

// Route to archiving a course
router.put("/:productId/archive", auth.verify, (req, res) => {
    let isAdmin = auth.decode(req.headers.authorization).isAdmin;
    productController.archiveProduct(req.params,req.body,isAdmin).then(resultFromController => res.send(resultFromController));

});
module.exports = router;