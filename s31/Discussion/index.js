// Use "require" directive to load Node.js modules
// A "module" lets Node.js transfer data using Hypertext Transfer Protocol
// Clients (browser) and servers (Node JS/Express JS Applications) Communicate by exchanging individual messages.

let http = require("http");

// Using this modules createServer() method, we can create an HTTP server that listens to requests on specified port.
// A port is a virtual point where network connection start and end.
// Each Port is associated with a specific process or service
//  The server will be assigned to port 4000
http.createServer(function (request, response){
	// Use writeHead() method to:
	// Set a status code for the message - 200 means ok
	// Set the content-typeof the response as a plain text
	response.writeHead(200, {"Content-Type" : "text/plain"});
	response.end("Hello World");
}).listen(4000);

console.log("Server running at localhost:4000");
