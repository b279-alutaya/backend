// 1. What directive is used by Node.js in loading the modules it needs?

Node.js uses the "require" directive to load the modules it needs.

// 2. What Node.js module contains a method for server creation?

The "http" module contains a method for server creation.
// 3. What is the method of the http object responsible for creating a server using Node.js?

The "createServer()" method of the "http" object is responsible for creating a server using Node.js.

// 4. What method of the response object allows us to set status codes and content types?
	
The "setHeader()" method of the response object allows us to set status codes and content types.

// 5. Where will console.log() output its contents when run in Node.js?

When console.log() is run in Node.js, its contents will be output to the console in the terminal where Node.js is running.

// 6. What property of the request object contains the address's endpoint?

The "url" property of the request object contains the endpoint address.