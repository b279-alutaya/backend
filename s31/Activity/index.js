const http = require("http");

// Creates a variable "port" to store the port number
const port = 3000;

const server = http.createServer((request, response) => {
	// Accessing the "greeting" route returns a message of "Hello Again"
	// "request" is an object that is sent via the client (browser)
	// The "url" property refers to the url or the link in the browser

	if(request.url == "/login"){
		response.writeHead(200, {"Content-Type" : "text/plain"});
		response.end("Welcome to the login page.");
	}else if(request.url == "/register"){
		response.writeHead(200, {"Content-Type" : "text/plain"});
		response.end("I'm sorry the page you are looking for cannot be found");
	} else{
		response.writeHead(404, {"Content-Type" : "text/plain"});
		response.end("404: Page not found!");
	}
});

server.listen(port);
console.log(`Server now accessible at localhost:${port}`);
