// Count the total number of fruits on sale
db.fruits.count({onSale: true})

// Count the total number of fruits with stock more than or equal to 20
db.fruits.count({stock: {$gte: 20}})

// Get the average price of fruits on sale per supplier
db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id", avgPrice: {$avg: "$price"}}}
])

// Get the highest price of a fruit per supplier
db.fruits.aggregate([
    {$group: {_id: {supplier: "$supplier_id", fruit: "$name"}, maxPrice: {$max: "$price"}}},
    {$group: {_id: "$_id.supplier", maxPrices: {$push: {fruit: "$_id.fruit", price: "$maxPrice"}}}}
])

// Get the lowest price of a fruit per supplier
db.fruits.aggregate([
    {$group: {_id: {supplier: "$supplier_id", fruit: "$name"}, minPrice: {$min: "$price"}}},
    {$group: {_id: "$_id.supplier", minPrices: {$push: {fruit: "$_id.fruit", price: "$minPrice"}}}}
])
