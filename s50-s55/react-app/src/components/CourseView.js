import {useState, useEffect, useContext} from "react"
import {Container, Card, Button, Col, Row} from "react-bootstrap"
import { useParams } from "react-router-dom"
import UserContext from "../UserContext"
import Swal from "sweetalert2"

export default function CourseView(){

	const {user} = useContext(UserContext);

	//Module that allows us to retrieve the courseId passed via Url
	const { courseId } = useParams();
	const [name, setName] = useState("");
	const [description, SetDescription] = useState("");
	const [price, setPrice] = useState("");

	useEffect(()=>{
		console.log(courseId);
		fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			SetDescription(data.description);
			setPrice(data.price);
		})
	},[courseId])

	const enroll = (courseId) =>{
		fetch(`${process.env.REACT_APP_API_URL}/users/enroll`,{
			method: "POST",
			headers: {
				"Content-type" : "application/json",
				Authorization : `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				courseId: courseId
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if (data === true){
				Swal.fire({
					title: "Successfully enrolled!",
					icon: 'success',
					text: "You have successfully enrolled for this course."
				});
			}else{
				if (data === true){
				Swal.fire({
					title: "Something went wrong",
					icon: 'error',
					text: "Please Try again."
				});
			}

			}
		})
	}
	return(
				<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							<Card.Subtitle>Class Schedule</Card.Subtitle>
							<Card.Text>8 am - 5 pm</Card.Text>
							<Button variant="primary"onClick={()=> enroll(courseId)}>Enroll</Button>
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>);
}