import courseData from "../data/courses"
import CourseCard from '../components/CourseCard'
import { useEffect, useState } from "react"

export default function Courses(){
	// Checks to see if mock data was captured
	console.log(courseData);
	console.log(courseData[0]);

	// State that will be used to store courses retrieved from db
	const [AllCourses, setAllCourses] = useState([]);

	//Retrieves the courses from database upon inital render of the "Courses" Component


	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/all`)
		.then(res=> res.json())
		.then(data => {
			console.log(data)

			setAllCourses(data.map(course => {
				return (
					<CourseCard key={course.id} courseProp={course}/>
					)
			}))
		})
	}, [])

	return(
		<>
			<h1>Courses</h1>
			{/*Prop making ang prop passing*/}
			{AllCourses}
		</>
		)
}