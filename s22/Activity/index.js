/*
    Create functions which can manipulate our arrays.
*/

/*
    Important note: Don't pass the arrays as an argument to the function. 
    The functions must be able to manipulate the current given arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

function register(username) {
  if (registeredUsers.includes(username)) {
    return "Registration failed. Username already exists!";
  } else {
    registeredUsers.push(username);
    return "Thank you for registering!";
  }
}

register("James Jeffries"); 
console.log(registeredUsers);


function addFriend(username) {
  if (registeredUsers.includes(username)) {
    friendsList.push(username);
    return `You have added ${username} as a friend!`;
  } else {
    return "User not found.";
  }
}

addFriend("James Jeffries");
console.log(friendsList);


function displayFriends() {
  if (friendsList.length === 0) {
    return "You currently have 0 friends. Add one first.";
  } else {
    friendsList.forEach((friend) => {
      console.log(friend);
    });
  }
}

displayFriends();



function displayNumberOfFriends() {
  if (friendsList.length === 0) {
    return "You currently have 0 friends. Add one first.";
  } else {
    return `You currently have ${friendsList.length} friends.`;
  }
}

console.log(displayNumberOfFriends());



function deleteFriend() {
if (friendsList.length === 0) {
return "You currently have 0 friends. Add one first.";
} else {
friendsList.pop();
}
}

deleteFriend();
console.log(friendsList);


/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/






//For exporting to test.js
try{
    module.exports = {

        registeredUsers: typeof registeredUsers !== 'undefined' ? registeredUsers : null,
        friendsList: typeof friendsList !== 'undefined' ? friendsList : null,
        register: typeof register !== 'undefined' ? register : null,
        addFriend: typeof addFriend !== 'undefined' ? addFriend : null,
        displayFriends: typeof displayFriends !== 'undefined' ? displayFriends : null,
        displayNumberOfFriends: typeof displayNumberOfFriends !== 'undefined' ? displayNumberOfFriends : null,
        deleteFriend: typeof deleteFriend !== 'undefined' ? deleteFriend : null

    }
} catch(err){

}