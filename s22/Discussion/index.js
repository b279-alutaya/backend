// console.log("Hello Wolrd!");

// [SECTION] Array Methods
// JS has built-in functions and methods for arrays. This allows us to manipulate and access array items.

// Mutator Methods
/*
    - Mutator methods are functions that "mutate" or change an array after they're created
    - These methods manipulate the original array performing various tasks such as adding and removing elements
*/

// push()

/*
    - Adds an element in the end of an array AND returns the array's length
    - Syntax
        arrayName.push();
*/

let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];

console.log("Current array:");
console.log(fruits);

let fruitsLength = fruits.push("Mango");
console.log(fruitsLength);

console.log("Mutated array from push method:");
console.log(fruits);

// Adding multiple values
fruits.push("Avocado", "Guava");
console.log("Mutated array from push method:");
console.log(fruits);

// pop()
/*
    - Removes the last element in an array AND returns the removed element
    - Syntax
        arrayName.pop();
*/

let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated array from pop method:");
console.log(fruits);

// unshift()
/*
    - Adds one or more elements at the beginning of an array
    - Syntax
        arrayName.unshift('elementA');
        arrayName.unshift('elementA', elementB);
*/

fruits.unshift("Lime", "Banana");
console.log("Mutated array from unshift method:");
console.log(fruits);

// shift()
/*
    - Removes an element at the beginning of an array AND returns the removed element
    - Syntax
        arrayName.shift();
*/

let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from shift method:");
console.log(fruits);

// splice()
/* 
    - Simultaneously removes elements from a specified index number and adds elements
    - Syntax
        arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
*/

fruits.splice(1, 2, "Lime", "Cherry");
console.log("Mutated array from splice method:");
console.log(fruits);

// sort()
/*
    - Rearranges the array elements in alphanumeric order
    - Syntax
        arrayName.sort();
*/

fruits.sort();
console.log("Mutated array from sort method:");
console.log(fruits);

// reverse()
/*
    - Reverses the order of array elements
    - Syntax
        arrayName.reverse();
*/
fruits.reverse();
console.log("Mutated array from reverse method:");
console.log(fruits);

// Non-mutator methods

let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];

// indexOf()
/*
    - Returns the index number of the first matching element found in an array
    - If no match was found, the result will be -1.
    - The search process will be done from first element proceeding to the last element
    - Syntax
        arrayName.indexOf(searchValue);
        arrayName.indexOf(searchValue, fromIndex);
*/

let firstIndex = countries.indexOf("PH");
console.log("Result from indexOf method: " + firstIndex);

let invalidCountry = countries.indexOf("BR");
console.log("Result from indexOf method: " + invalidCountry);

// lastIndexOf()
/*
    - Returns the index number of the last matching element found in an array
    - The search process will be done from last element proceeding to the first element
    - Syntax
        arrayName.lastIndexOf(searchValue);
        arrayName.lastIndexOf(searchValue, fromIndex);
*/

let lastIndexOf = countries.lastIndexOf("PH");
console.log("Result from indexOf method: " + lastIndexOf);

let lastIndexStart = countries.lastIndexOf("PH", 1);
console.log("Result from indexOf method: " + lastIndexStart);

// Slice ()
/*
    - Portions/slices elements from an array AND returns a new array
    - Syntax
        arrayName.slice(startingIndex);
        arrayName.slice(startingIndex, endingIndex);
*/

// Slicing off elements from a specified index to the last element
let slicedArrayA = countries.slice(2);
console.log("Result from slice method:");
console.log(slicedArrayA);

let slicedArrayB = countries.slice(2, 4);
console.log("Result from slice method:");
console.log(slicedArrayB);

let slicedArrayC = countries.slice(-3);
console.log("Result from slice method:");
console.log(slicedArrayC);

// toString()
/*
    - Returns an array as a string separated by commas
    - Syntax
        arrayName.toString();
*/
let stringArr = countries.toString();
console.log("Result from toString method:");
console.log(stringArr);

// concat()
/*
    - Combines two arrays and returns the combined result
    - Syntax
        arrayA.concat(arrayB);
        arrayA.concat(elementA);
*/

let tasksArrayA = ['drink html', 'eat javascript'];
let tasksArrayB = ['inhale css', 'breathe sass'];
let tasksArrayC = ['get git', 'be node'];

let tasks = tasksArrayA.concat(tasksArrayB);
console.log("Result from concat method:");
console.log(tasks);

// Combining arrays with elements
let combinedTasks = tasksArrayA.concat("smell express", "throw react");
console.log("Result from concat method:");
console.log(combinedTasks);

// Join ()
/*
    - Returns an array as a string separated by specified separator string
    - Syntax
        arrayName.join('separatorString');
*/
let users = ["John", "Jane", "Joe", "Robert"];
console.log(users.join());
console.log(users.join(""));
console.log(users.join(" - "));

// Iteration
// forEach()
/*
    - Similar to a for loop that iterates on each array element.
    - For each item in the array, the anonymous function passed in the forEach() method will be run.
    - The anonymous function is able to receive the current item being iterated or loop over by assigning a parameter.
    - Variable names for arrays are normally written in the plural form of the data stored in an array
    - It's common practice to use the singular form of the array content for parameter names used in array loops
    - forEach() does not return anything.
    - Syntax
        arrayName.forEach(function(indivElement) {
            statement
        })
*/

tasks.forEach(function(task){
	console.log(task);

});

// Using forEach with conditional statements
let filteredTasks = [];
tasks.forEach(function(){
	if(tasks.length > 10){
		filteredTasks.push(task);
	}
});
console.log("Results from filtered tasks: ");
console.log(filteredTasks);

// map()
/* 
    - Iterates on each element AND returns new array with different values depending on the result of the function's operation
    - This is useful for performing tasks where mutating/changing the elements are required
    - Unlike the forEach method, the map method requires the use of a "return" statement in order to create another array with the performed operation
    - Syntax
        let/const resultArray = arrayName.map(function(indivElement))
*/

let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function(number){
	return number * number;

});

console.log("Original Array");
console.log(numbers);
console.log("Result from map method");
console.log(numberMap);

 let numberForEach = numbers.forEach(function(number){
 		return number * number;
 });

 console.log(numberForEach);

 // every()
/*
    - Checks if all elements in an array meet the given condition
    - This is useful for validating data stored in arrays especially when dealing with large amounts of data
    - Returns a true value if all elements meet the condition and false if otherwise
    - Syntax
        let/const resultArray = arrayName.every(function(indivElement) {
            return expression/condition;
        })
*/

 let allValid = numbers.every(function(number){
 	return (number < 10);

 });

 console.log("Result of every method");
 console.log(allValid);

 // some()

 /*
    - Checks if at least one element in the array meets the given condition
    - Returns a true value if at least one element meets the condition and false if otherwise
    - Syntax
        let/const resultArray = arrayName.some(function(indivElement) {
            return expression/condition;
        })
*/

 // filter ()