// Get post data

fetch("https://jsonplaceholder.typicode.com/posts")
.then(response => response.json())
.then(data => showPost(data));

// Add post data
// Document object manipulation

document.getElementById("form-add-post")
.addEventListener("submit", e => {

	// prevents our webpage to having unwanted "refresh" after listening to an event
	e.preventDefault();

	fetch("https://jsonplaceholder.typicode.com/posts", {
		method: "POST",
		body: JSON.stringify({
			title: document.getElementById("txt-title").value,
			body: document.getElementById("txt-body").value,
			userId: 1
		}),
		headers: {"Content-type" : "application/json; charset=UTF-8"}

	})
	.then(res => res.json())
	.then(data => {
		console.log(data);
		alert("Successfully added!");

		document.getElementById("txt-title").value = null;
		document.getElementById("txt-body").value = null;
	})

})

// Show all posts

const showPost = (posts) => {
	let postEntries = "";

	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>

		`;
	})


	document.getElementById("div-post-entries").innerHTML = postEntries;
}

// Edit post

const editPost = (id) => {	
	let title = document.getElementById(`post-title-${id}`).innerHTML;
	let body = document.getElementById(`post-body-${id}`).innerHTML;

	// Passing of data in the Edit post section

	document.getElementById("txt-edit-id").value = id;
	document.getElementById("txt-edit-title").value = title;
	document.getElementById("txt-edit-body").value = body;
	document.getElementById("btn-submit-update").removeAttribute("disabled");
}

//Updating a post

document.getElementById("form-edit-post").addEventListener("submit", e => {
    e.preventDefault();

    fetch("https://jsonplaceholder.typicode.com/posts/1", {
        method: "PUT",
        body: JSON.stringify({
            id: document.getElementById("txt-edit-id").value,
            title: document.getElementById("txt-edit-title").value,
            body: document.getElementById("txt-edit-body").value,
            userId: 1
        }),
        headers: { "Content-type": "application/json; charset=UTF-8" }
    })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            alert("Successfully Updated!");
            document.getElementById("txt-edit-id").value = null;
            document.getElementById("txt-edit-title").value = null;
            document.getElementById("txt-edit-body").value = null;
            document.getElementById("btn-submit-update").setAttribute("disabled", true);
        });
});
const deletePost = (id) => {
    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
        method: "DELETE"
    })
    .then(res => {
        const postElement = document.getElementById(`post-${id}`);
        postElement.remove();
    })
    .then(data => {
        alert("Successfully Deleted!");
    })
    .catch(error => {
        console.error("Error:", error);
        alert("An error occurred while deleting the post.");
    });
};






