// Number 3
fetch("https://jsonplaceholder.typicode.com/todos")
.then(res => res.json())
.then(json => console.log(json));
// Number 4
fetch("https://jsonplaceholder.typicode.com/todos")
  .then(res => res.json())
  .then(json => {
    const titles = json.map(item => item.title);
    console.log(titles);
  });
// Number 5
fetch("https://jsonplaceholder.typicode.com/todos/1")
  .then(res => res.json())
  .then(json => console.log(json));
// Number 6
fetch("https://jsonplaceholder.typicode.com/todos/1")
  .then(res => res.json())
  .then(json => console.log(`Title: ${json.title}, Status: ${json.completed ? 'Completed' : 'Incomplete'}`));

// Number 7
fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
		title: "New Todo Item",
		completed: false,
    	userId: 1
	})
	
})
.then(res => res.json())
.then(json => console.log(json));
// Number 8
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
		title: "Updated New Todo Item",
		completed: true,
    	userId: 1
	})
	
})
.then(res => res.json())
.then(json => console.log(json));


// Number 9

fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "PUT",
  body: JSON.stringify({
    title: "New Title",
    description: "New Description",
    status: "Incomplete",
    completedAt: "2023-05-03T14:30:00.000Z",
    userId: 1
  }),
  headers: {
    "Content-type": "application/json; charset=UTF-8"
  }
})
  .then(res => res.json())
  .then(json => console.log(json));


 // Number 10

  fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
		title: "Updated Title"
	})
})
.then(res => res.json())
.then(json => console.log(json));

// Number 11
fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "PUT",
  body: JSON.stringify({
    title: "New Title",
    description: "New Description",
    status: "Completed",
    completedAt: "2023-05-03T14:59:00.000Z",
    userId: 1
  }),
  headers: {
    "Content-type": "application/json; charset=UTF-8"
  }
})
  .then(res => res.json())
  .then(json => console.log(json));
  
  // Number 12
  fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "DELETE"
});